/**
 * Simple class to read a CSV and make available a data structure holding said data
 */
package me.jcsims;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * @author Chris Sims
 *
 */
public class CsvReader {
	
	private Vector<Vector<Double>> data;

	public Vector<Vector<Double>> getData() {
		return data;
	}
	
	CsvReader() {
		data = new Vector<Vector<Double>>();
	}

	/**
	 * Read a CSV file and build a data set.
	 * 
	 * File format is expected to follow:
	 * atr1, attr2, ..., attrn, clasification
	 * 
	 * Attributes and the classification are expected to be floating point numbers
	 * 
	 * @param filename
	 * @throws IOException
	 */
	public void readFile(String filename) throws IOException {
		BufferedReader in = null;
		List<String> lines = null;
		try {
			in = new BufferedReader(new FileReader(filename));
			lines = new ArrayList<String>();
			String line = null;
			while ((line = in.readLine()) != null) {
				lines.add(line);
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(in != null){
				in.close();
			}
		}
		
		if (lines != null) {
			buildDataSet(lines);
		}
	}

	/**
	 * Build a vector of vectors that represents the data set
	 * @param lines 
	 */
	private void buildDataSet(List<String> lines) {
		for (String line : lines) {
			Vector<Double> newV;
			String[] splitLine = line.split(",");
			newV = new Vector<Double>(splitLine.length);
			for (int i = 0; i < splitLine.length; i++) {
				newV.add(Double.parseDouble(splitLine[i]));
			}
			data.add(newV);
		}	
	}
}

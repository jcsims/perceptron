/**
 * Synthesizes a data set
 */
package me.jcsims;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.Vector;

/**
 * @author Chris Sims
 *
 */
public class DataSet {
	private Vector<Double> w;
	private double b;
	
	private int length;
	private int count;
	private double lowerBound;
	private double upperBound;
	
	private Vector<Vector<Double>> dataSet;

	public Vector<Vector<Double>> getDataSet() {
		return dataSet;
	}

	/**
	 * Builds a data set given some parameters
	 * @param w
	 * 		Normal vector for the intended decision surface
	 * @param b
	 * 		Offset for decision surface
	 * @param lowerBound
	 * 		Lower bound for the data points (per dimension)
	 * @param upperBound
	 * 		Upper bound for the data points (per dimension)
	 * @param length
	 * 		Length of each vector (no. of dimensions per data point). Does not count the classification.
	 * 		I.e. vectors will be of length: <code>length + 1 </code> after the set is generated
	 * @param count
	 * 		Number of observations
	 */
	DataSet(Vector<Double> w, double b, double lowerBound, double upperBound, int length, int count) {
		if (lowerBound >= upperBound) {
			throw new IllegalArgumentException("lowerBound must be less than upperBound");
		}
		
		this.w = w;
		this.b = b;
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
		this.length = length;
		this.count = count;
		
		dataSet = new Vector<Vector<Double>>(count);
		
		buildDataSet();
	}

	private void buildDataSet() {
		double range = upperBound - lowerBound;
		Random rand = new Random();
				
		for (int i = 0; i < count; i++) {
			//Build each vector
			Vector<Double> vector = new Vector<Double>(length + 1);
			for (int j = 0; j < length; j++) {
				vector.add(rand.nextDouble() * range + lowerBound);
			}
			vector = classify(vector);
			dataSet.add(vector);
		}
	}

	private Vector<Double> classify(Vector<Double> vector) {
		double classification = Math.signum(VectorOps.dot(vector, w) - b);
		vector.insertElementAt(classification, vector.size());
		return vector;
	}
	
	public void writeToFile(String filename) {
		try {
			FileWriter fstream = new FileWriter(filename);
			BufferedWriter out = new BufferedWriter(fstream);
			
			for (Vector<Double> vector : dataSet) {
				out.write(vector.toString() + '\n');
			}
			
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

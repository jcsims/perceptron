/**
 * 
 */
package me.jcsims;

import java.util.Random;
import java.util.Vector;

/**
 * @author Chris Sims
 *
 */
public class Main {
	
	public static final String DATA_FILE = "data/magic04.csv";
	public static final String OUT_FILE = "out/data_out.csv";
	public static final double LEARNING_RATE = 0.3;

	/**
	 * @param args Command-line arguments
	 */
	public static void main(String[] args) {
		Random rand = new Random();
		Vector<Double> w = generateDecisionSurface(10, rand);
		double b = rand.nextDouble() * 10;
		
		long startTime = System.nanoTime();
		DataSet dataset = new DataSet(w, b, -10.0, 10.0, 10, 20000);
		long elapsedTime = System.nanoTime() - startTime;
		System.out.println("Time to build data set: " + elapsedTime/1000000 + "ms");

		
		Perceptron model = new Perceptron();
		startTime = System.nanoTime();
		model.buildModel(dataset.getDataSet(), LEARNING_RATE);
		elapsedTime = System.nanoTime() - startTime;
		System.out.println("Time to build model: " + elapsedTime/1000000 + "ms");
		
		
		
		//testing
		{
			testModel(model, dataset.getDataSet());
		}
	}

	/**
	 * Test a generated model against a data set
	 * @param model
	 * @param dataSet
	 * 		Should have the same number of attributes used to train the model, plus an extra
	 * 		column for the classification.
	 */
	private static void testModel(Perceptron model, Vector<Vector<Double>> dataSet) {
		long startTime = System.nanoTime();
		for (Vector<Double> vector : dataSet) {
			double classification = vector.remove(vector.size() -1);
			double f = model.classify(vector);
			if (f != classification) {
				System.out.println("No match!");
			}
		}
		long elapsedTime = System.nanoTime() - startTime;
		System.out.println("Time to classify data set: " + elapsedTime/1000000 + "ms");
	}

	/**
	 * Generates a random decision surface, with values between -10 and 10
	 * 
	 * @param dimensions Vector dimensions
	 * @param rand Random number generator
	 * @return
	 */
	private static Vector<Double> generateDecisionSurface(int dimensions, Random rand) {
		Vector<Double> w = new Vector<Double>(dimensions);
		for (int i = 0; i < dimensions; i++) {
			w.add(rand.nextDouble() * 20 - 10);
		}
		return w;
	}

}

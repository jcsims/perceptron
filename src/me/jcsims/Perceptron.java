/**
 * 
 */
package me.jcsims;

import java.util.Vector;

/**
 * @author Chris Sims
 *
 */
public class Perceptron {
	private Vector<Double> w;
	private double b;
	private double r;
	
	public Vector<Double> getW() {
		return w;
	}

	Perceptron() {
		w = new Vector<Double>();
		b = 0.0;
		r = 0.0;
	}
	
	/**
	 * Builds a perceptron model based on a data set
	 * @param data
	 * @param learningRate Rate at which each iteration will affect the proposed decision surface
	 */
	public void buildModel(Vector<Vector<Double>> data, double learningRate) {
		r = radius(data);
		
		int j = data.get(0).size() -1;
		//Ensure that the w vector is a proper zero vector
		for (int i = 0; i < j; i++) {
			w.add(0.0);
		}
		
		//Start looking for the optimal decision surface
		boolean found = false;
		while (!found) {
			found = true;
			for (Vector<Double> vector : data) {
				double classification = vector.remove(vector.size() - 1);
				if (Math.signum(VectorOps.dot(w, vector) - b) != classification) {
					w = VectorOps.add(w, VectorOps.scalarMult(learningRate * classification, vector));
					b = b - (learningRate * classification * Math.pow(r, 2));
					found = false;
				}
				vector.add(classification);
			}
		}
	}
	
	/**
	 * Classifies a given observation
	 * @param observation
	 * @return
	 * 		Either -1.0 or 1.0, based on the training data set.
	 */
	public double classify(Vector<Double> observation) {
		return Math.signum(VectorOps.dot(observation, w) - b);
	}
	
	/**
	 * Determines the max radius for a give data set
	 * @param data
	 * @return Max radius
	 */
	private double radius(Vector<Vector<Double>> data) {
		double ret = 0;
		for (Vector<Double> vector : data) {
			double length = VectorOps.norm(vector);
			ret = length > ret ? length : ret;
		}
		return ret;
	}
	

}

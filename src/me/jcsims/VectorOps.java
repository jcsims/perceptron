package me.jcsims;

import java.util.Vector;

public class VectorOps {

	/**
	 * Calculate the Euclidean norm of a given vector
	 * @param vector
	 * @return
	 */
	public static double norm(Vector<Double> vector) {
		double sum = 0;
		for (Double element : vector) {
			sum += (element * element);
		}
		return Math.sqrt(sum);
	}
	
	/**
	 * Calculate the dot product of two vectors of the same length
	 * @param a 
	 * @param b
	 * @return
	 */
	public static double dot(Vector<Double> a, Vector<Double> b) {
		if (a.size() != b.size()) {
			throw new IllegalArgumentException("Both vectors need to be the same length: a:" + a.size() + " b: " + b.size());
		}
		double sum = 0;
		for (int i = 0; i < a.size(); i++) {
			sum += a.get(i) * b.get(i);
		}
		return sum;
	}

	public static Vector<Double> scalarMult(double scalar, Vector<Double> vector) {
		Vector<Double> newVector = new Vector<Double>(vector.size());
		for (Double element : vector) {
			newVector.add(scalar * element);
		}
		return newVector;
	}

	public static Vector<Double> add(Vector<Double> a, Vector<Double> b) {
		if (a.size() != b.size()) {
			throw new IllegalArgumentException("Vectors must be of the same size.");
		}
		
		Vector<Double> result = new Vector<Double>(a.size());
		
		for (int i = 0; i < a.size(); i++) {
			result.add(a.get(i) + b.get(i));
		}
		return result;
	}
	
}
